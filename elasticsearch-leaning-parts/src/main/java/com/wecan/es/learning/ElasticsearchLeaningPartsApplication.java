package com.wecan.es.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchLeaningPartsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchLeaningPartsApplication.class, args);
    }

}
