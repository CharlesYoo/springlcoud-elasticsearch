package com.wecan.es.learning.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * indexName：对应索引库名称（相当于数据库）
 * type：对应在索引库中的类型（相当于表）
 * shards：分片数量，默认5
 * replicas：副本数量，默认1
 */
@Setter
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Mapping(mappingPath = "elasticsearch_mapping.json")//设置mapping
@Setting(settingPath = "elasticsearch_setting.json")//设置setting
@ApiModel(value = "货物")
@Document(indexName = "goods", type = "good", shards = 5, replicas = 1)
public class Good implements Serializable {
    /**
     * id
     */
    @ApiModelProperty(value = "获取编号")
    @Id
    private Long id;
    /**
     * type：字段类型，是枚举：FieldType，可以是text、long、short、date、integer、object等
     * text：存储数据时候，会自动分词，并生成索引
     * keyword：存储数据时候，不会分词建立索引
     * Numerical：数值类型，分两类
     * 基本数据类型：long、interger、short、byte、double、float、half_float
     * 浮点数的高精度类型：scaled_float
     * 需要指定一个精度因子，比如10或100。elasticsearch会把真实值乘以这个因子后存储，取出时再还原。
     * Date：日期类型
     * elasticsearch可以对日期格式化为字符串存储，但是建议我们存储为毫秒值，存储为long，节省空间。
     * index：是否索引，布尔类型，默认是true
     * store：是否存储，布尔类型，默认是false
     * analyzer：分词器名称，这里的ik_max_word即使用ik分词器
     */
    /**
     * 名称
     */
    @ApiModelProperty(value = "名字")
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String title;
    /**
     * 分类
     */
    @ApiModelProperty("分类")
    @Field(type = FieldType.Keyword)
    private String category;// 分类
    /**
     *品牌
     *
     */
    @ApiModelProperty("品牌")
    @Field(type = FieldType.Text,analyzer = "pinyin_analyzer",searchAnalyzer = "pinyin_analyzer")
    private String brand; // 品牌
    /**
     * 价格
     */
    @ApiModelProperty("价格")
    @Field(type = FieldType.Double)
    private Double price; // 价格
    /**
     * 图片
     */
    @ApiModelProperty("图片")
    @Field(index = false, type = FieldType.Keyword)
    private String images; // 图片地址


}
