package com.wecan.es.learning.repository;

import com.yyh.elasticsearch.entity.Good;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

@Component
public interface GoodsRepository extends ElasticsearchRepository<Good, Long> {
}
