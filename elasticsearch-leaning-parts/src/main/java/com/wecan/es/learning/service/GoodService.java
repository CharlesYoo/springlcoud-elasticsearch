package com.wecan.es.learning.service;

import com.alibaba.fastjson.JSONObject;
import com.yyh.elasticsearch.entity.Good;

import java.util.List;
import java.util.Map;

public interface GoodService {
    /**
     * @param good
     * @return String
     * @Title saveGoods
     * @Description: 保存商品
     * @author yaoyonghao
     * @date 2019/3/5 14:22
     */
    void saveGoods(Good good);

    /**
     * @return com.alibaba.fastjson.JSONObject;
     * @Title findAll
     * @Description: 查询所有
     * @author yaoyonghao
     * @date 2019/3/5 14:25
     */
    JSONObject findAll();

    /**
     * @param id
     * @return void
     * @Title deleteGoodById
     * @Description: 通过id删除商品
     * @author yaoyonghao
     * @date 2019/3/5 14:27
     */
    void deleteGoodById(Long id);

    /**
     * @param goodinfo
     * @return void
     * @Title updateGoods
     * @Description: 更新商品信息
     * @author yaoyonghao
     * @date 2019/3/5 14:35
     */
    void updateGoods(Good goodinfo);

    /**
     * @param pageNum  pageNumber
     * @param pageSize pagesize
     * @param query    查询条件
     * @return com.alibaba.fastjson.JSONObject;
     * @Title queryListByPage
     * @Description: 分页模糊查询
     * @author yaoyonghao
     * @date 2019/3/5 14:38
     */
    JSONObject queryListByPage(Integer pageNum, Integer pageSize, String query);

    /**
     * @param searchContent 查询条件
     * @return com.alibaba.fastjson.JSONObject;
     * @Title searchContent
     * @Description: 模糊查询
     * @author yaoyonghao
     * @date 2019/3/5 14:41
     */
    JSONObject searchContent(String searchContent);

    /**
     * @param title    题目
     * @param brand    品牌
     * @param pageNum  位置
     * @param pageSize  大小
     * @param sort 正序，降序
     * @return com.alibaba.fastjson.JSONObject;
     * @Title searchComplexByCondition
     * @Description: 非聚合组合条件查询
     * @author yaoyonghao
     * @date 2019/3/5 14:49
     */
    JSONObject searchComplexByCondition(String title, String brand, Integer pageNum, Integer pageSize, String sort);
    /**
    * @Title queryHit
    * @Description: 查询高亮
    * @param
    * @return List<Map<String,Object>>
    * @author yaoyonghao
    * @date 2019/3/5 15:00
    */
    List<Map<String,Object>> queryHit(String keyword, String indexName, String... fieldNames);

   /**
   * @Title
   * @Description: 通过中文分词器模糊查询数据
   * @param
   * @return
   * @author yaoyonghao
   * @date 2019/6/26 10:21
   */
    JSONObject searchByPinyin(String brand);
}
