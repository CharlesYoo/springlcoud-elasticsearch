package com.wecan.es.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchLearningClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchLearningClientApplication.class, args);
    }

}
