package com.wecan.es.client.entity;


import lombok.Data;

/**
 * 课程模型对象
 *
 */
@Data
public class Course {

    private String id;

    //课程标题
    private String courseTitle;

    //课程讲师
    private String courseTeacher;

    //课程热度
    private String courseHot;

    //课程价格
    private String coursePrice;

    //课程图片
    private String courseImage;

    //课程链接
    private String courseLink;
}