package com.wecan.es.data.controller;

import com.wecan.es.data.entity.Course;
import com.wecan.es.data.service.CourserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class CousreController {
    @Autowired
    private CourserService courserService;

    /**
     * 添加
     *
     * @param course
     * @return
     */
    @PostMapping("/insert")
    public Course insertCourse(Course course) {
        return courserService.insertCourse(course);
    }

    /**
     * 查询
     * @param param
     * @return
     */
    @GetMapping
    public List<Course> searchCourse(@PathVariable String param) {
    return  courserService.search(param);
    }

    /**
     * 分页查询
     * @param page
     * @param size
     * @param param
     * @return
     */
    @GetMapping("/{page}/{size}/{param}")
    public List<Course> searchCoursePage(@PathVariable Integer page,@PathVariable Integer size,@PathVariable String param){
        return courserService.searchPage(page,size,param);
    }
    @DeleteMapping("/delete/{id}")
    public void deleteCourse(@PathVariable String id){
        courserService.deleteCourse(id);
    }
    @PutMapping("/update")
    public Course updateCourse(Course course){
        return  courserService.updateCourse(course);
    }
}
