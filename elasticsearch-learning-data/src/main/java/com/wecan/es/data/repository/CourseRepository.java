package com.wecan.es.data.repository;

import com.wecan.es.data.entity.Course;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CourseRepository extends ElasticsearchRepository<Course,String> {
}
