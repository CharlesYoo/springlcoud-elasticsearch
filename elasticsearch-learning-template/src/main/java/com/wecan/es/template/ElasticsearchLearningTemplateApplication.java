package com.wecan.es.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchLearningTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchLearningTemplateApplication.class, args);
    }

}
