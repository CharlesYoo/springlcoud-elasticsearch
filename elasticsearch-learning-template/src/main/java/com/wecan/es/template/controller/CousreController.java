package com.wecan.es.template.controller;

import com.wecan.es.template.entity.Course;
import com.wecan.es.template.service.CourserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class CousreController {
    @Autowired
    private CourserService courserService;

    /**
     * 添加
     *
     * @param course
     * @return
     */
    @PostMapping("/insert")
    public String insertCourse(Course course) {
        return courserService.insertCourse(course);
    }

    /**
     * 通过id 进行查询
     *
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public Object getCourseInfoById(@PathVariable String id) {
        return courserService.getCourseInfoById(id);
    }

    /**
     * 通过条件全文检索
     *
     * @param param
     * @return
     */
    @GetMapping("/select/{params}")
    public List<Course> search(@PathVariable String param) {
        return courserService.searchCourseList(param);
    }

    @GetMapping("/{page}/{size}/{param}")
    public List<Course> searchCourseInfoByPage(@PathVariable Integer page, @PathVariable Integer size, @PathVariable String param) {
        return courserService.searchPage(page, size, param);

    }

    /**
     * 通过id 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delet/{id}")
    public Object deleteCourse(@PathVariable String id) {
        courserService.deleteCourseByid(id);
        return id;
    }

    @PutMapping("/update")
    public Object updateCourse(Course course) {
        return courserService.updateCourse(course);

    }
}
