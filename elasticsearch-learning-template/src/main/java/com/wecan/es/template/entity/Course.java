package com.wecan.es.template.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * 课程模型对象
 */
@Document(indexName = "course", shards = 5, replicas = 1)
public class Course {

    @Id
    private String id;

    //课程标题
    private String courseTitle;

    //课程讲师
    private String courseTeacher;

    //课程热度
    private String courseHot;

    //课程价格
    private String coursePrice;

    //课程图片
    private String courseImage;

    //课程链接
    private String courseLink;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //课程介绍
    private String courseIntroduce;

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getCourseTeacher() {
        return courseTeacher;
    }

    public void setCourseTeacher(String courseTeacher) {
        this.courseTeacher = courseTeacher;
    }

    public String getCourseHot() {
        return courseHot;
    }

    public void setCourseHot(String courseHot) {
        this.courseHot = courseHot;
    }

    public String getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(String coursePrice) {
        this.coursePrice = coursePrice;
    }

    public String getCourseImage() {
        return courseImage;
    }

    public void setCourseImage(String courseImage) {
        this.courseImage = courseImage;
    }

    public String getCourseLink() {
        return courseLink;
    }

    public void setCourseLink(String courseLink) {
        this.courseLink = courseLink;
    }

    public String getCourseIntroduce() {
        return courseIntroduce;
    }

    public void setCourseIntroduce(String courseIntroduce) {
        this.courseIntroduce = courseIntroduce;
    }
}