package com.wecan.es.template.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wecan.es.template.entity.Course;
import netscape.javascript.JSObject;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CourserService {
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    public String insertCourse(Course course) {
        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(course.getId())
                .withObject(course)
                .build();
        String documentId = elasticsearchRestTemplate.index(indexQuery);
        return documentId;
    }

    public Object getCourseInfoById(String id) {
        return elasticsearchRestTemplate.queryForObject(GetQuery.getById(id), Course.class);
    }

    public List<Course> searchCourseList(String param) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.queryStringQuery(param).defaultField("courserTitle"))
                .build();
        List<Course> courses = elasticsearchRestTemplate.queryForList(nativeSearchQuery, Course.class);
        if (courses != null)
            return courses;
        else
            return null;
    }

    public List<Course> searchPage(Integer page, Integer size, String param) {
        //分页
        Pageable pageable = PageRequest.of(page, size);
        //构建查询条件
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .should(QueryBuilders.matchQuery("courseTitle", param))
                .should(QueryBuilders.matchQuery("courseIntroduce", param))
                .should(QueryBuilders.matchQuery("courseTeacher", param));
        //
        NativeSearchQuery nativeSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(pageable)
                .build();
        List<Course> courses = elasticsearchRestTemplate.queryForList(nativeSearchQuery, Course.class);
        if (courses != null && !courses.isEmpty())
            return courses;
        else
            return null;
    }

    public void deleteCourseByid(String id) {
        elasticsearchRestTemplate.delete(Course.class, id);
    }

    public Object updateCourse(Course course) {
        UpdateRequest updateRequest = new UpdateRequest();

        JSONObject jsonObject = (JSONObject) JSON.toJSON(course);
        Set<Map.Entry<String, Object>> entrySet = jsonObject.entrySet();
        Map<String, Object> map = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : entrySet) {
            map.put(entry.getKey(), entry.getValue());
        }
        updateRequest.doc(map);

        UpdateQuery updateQuery = new UpdateQueryBuilder()
                .withClass(Course.class)
                .withUpdateRequest(updateRequest)
                .withId(course.getId())
                .build();

        //真正的更新
        UpdateResponse response = elasticsearchRestTemplate.update(updateQuery);
        return response.status().getStatus();
    }
}
