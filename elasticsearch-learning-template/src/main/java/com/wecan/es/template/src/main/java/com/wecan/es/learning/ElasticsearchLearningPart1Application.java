package com.wecan.es.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchLearningPart1Application {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchLearningPart1Application.class, args);
    }

}
